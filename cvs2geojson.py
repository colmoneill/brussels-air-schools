import csv
import json

csvfile = "/home/colm/git/OSP/medor.carto.pollution/export_etablissements/maternel_specialise.csv"
csvfile = open(csvfile, 'r')
#csvfile = csvfile.encode('utf-8').strip()
jsonfile = "/home/colm/git/OSP/medor.carto.pollution/export_etablissements/maternel_specialise.json"
jsonfile = open(jsonfile, 'w')

def getJSONFromTestoCSV(csvfile, jsonfile):
    fieldnames = ("ID", "date", "temperature", "humidity")
    reader = csv.DictReader(csvfile, fieldnames, delimiter=';', quotechar='|')
    for idx, row in enumerate(reader):
        if (idx != 0):
            row = row.encode('utf-8').strip()
            print(row)
            json.dump(row, jsonfile)
            jsonfile.write('\n')

getJSONFromTestoCSV(csvfile, jsonfile)
