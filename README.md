# Brussels pollution carto

Open layers project conflating Brussels pollution data (NO² levels) with school zones and locations.

![](images/web1.jpg)

![](images/web2.jpg)

![](images/_6.png)

<hr>

to run the project:
* ``cd air/``
* ``npm install``
* ``npm start``
* visit localhost:3000



<hr>

### Notes from osp blog draft:

Journalists from Médor is continuing to report on the problematic situation of air quality in Belgium and Brussels. A recent development propted the need for mapping and visualisation of publicly available pollution (NO² gaz levels) data in school zones in Wallonie and Brussels. This article details how the data was processed, how the queries were made, and how the map was assembled.

### Outline:
1. Geocoding school locations
2. Handeling the datasource
3. Bringing them together  

![](images/9.png)

## 1. Geocoding school locations

A request was made to [enseignement.be](http://enseignement.be/index.php) to obtain a list of addresses of all the schools in the Communauté Française. The organism provided us with 7 CSV files that gathered schools by level and by tipes. The cvs format includes the name of the school, an address and contact information for each institution. In order to be able to plot each school on a map, we needed to have latitude and longitude coordinates for each address. This process is called geocoding, and can be done in multiple ways. Quite a lot of websites propose this as a (paying) service, but in our case, premiliminary test on [Nominatim](https://nominatim.openstreetmap.org/) (the OSM data search engine) prooved to be very accurate. I made a very simple python script that queries nominatim with the addresses extracted from the csv files and returns latitude and logitude to two new columns in the output document.

Nominatim did not return results for all addresses. If Nominatim could not find a result, I did an attempt with google's query, and if that still failed, arcGis seemed to always do the trick. This is all made extra easy thanks to the [geocoder](http://geocoder.readthedocs.io/) library which is under the MIT licence. Thanks for this great tool!

I prioritised nominatim because I have a soft spot for anything OSM. It's clearly not the best for geocoding, but I knew we would be using OSM tiles as a basemap in the end project, so I thought there was some sense there. One of the downfalls of using nominatim's main server for queries is that you are limited to one request per second, which is totally normal and legitimate, but I still found out about it the hard way:

 ![](images/2.png)


<code>

    import csv
    import sys
    import geocoder
    import time

    ifile  = open(sys.argv[1])
    reader = csv.reader(ifile)
    ofile  = open(sys.argv[2], "w")
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    rownum = 0
    for row in reader:
        # Save header row.
        if rownum == 0:
            header = row
            print(header)
            header.extend(['lat', 'lng'])
            print(header)
            writer.writerow(row)

        else:

            time.sleep(1)
            colnum = 0
            query = row[1] + ', ' + row[2] + ', ' + row[3] + ', ' + row[4]
            print('query= ' + query)

            g = geocoder.osm(query)
            if g.ok is False:
                #print(row[0])
                print(query + 'no result with OSM')
                #print(g.json)

                g = geocoder.google(query)
                if g.ok is False:
                    print(query + 'returns no result with google')
                    g = geocoder.arcgis(query)

                    if g.ok is False:
                        print('NO RESULTS USING OSM THEN GOOGLE THEN ARCGIS')
                    else:
                        print('result found with ARCGIS: ')
                        print(g.latlng)
                print('final result:')
                print(g.latlng)

            print(g.latlng)
            #print(row)
            row.extend([g.lat, g.lng])
            print(row)
            writer.writerow(row)
            #for col in row:
                #print(col)
            #    colnum += 1

        rownum += 1
        print('line:' + str(rownum))

    ifile.close()
    ofile.close()

</code>

## 2. Handeling the datasource

Once all the addresses had latlng data available, all of the heavy lifiting was done in [QGIS](https://qgis.org/en/site/). The pollution data itself came form [IRCELINE](http://www.irceline.be/) the belgian authority that surveils gathers and publishes atmospheric emissions data. We worked from the annual nation wide NO² reports for the year 2016, while we wait for the 2017 reports to be issued.

It very important to say that these maps are representations, they are modelings of measures, averaged over the year.http://www.irceline.be/en/air-quality/measurements/nitrogen-dioxide/history/no2_anmean_rioifdm?set_language=en   

## 3. Briging them together:

Tiles provided by tile.osm.be

<hr>

article and notes ongoing
