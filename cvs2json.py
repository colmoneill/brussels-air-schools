# -*- coding: utf-8 -*-
import csv
import unicodecsv as csv
import json
import glob
import os
from isounidecode import unidecode

#file_contents =   unidecode(file_stream.read())

for filename in glob.glob('/home/colm/git/OSP/medor.carto.pollution/export_etablissements/*.csv'):
    print(filename)
    csvfile = os.path.splitext(filename)[0]
    jsonfile = csvfile + '.json'
    print(jsonfile)

    with open(csvfile+'.csv') as f:
        reader = csv.DictReader(f)
        rows = list(reader)

    with open(jsonfile, 'w') as f:
        json.dump(rows, f)
