import 'ol/ol.css';
import Map from 'ol/map';
import View from 'ol/view';
import TileLayer from 'ol/layer/tile';
import XYZSource from 'ol/source/xyz';
import proj from 'ol/proj';
import VectorLayer from 'ol/layer/vector';
import VectorSource from 'ol/source/vector';
import GeoJSON from 'ol/format/geojson';
import Style from 'ol/style/style';
import Fill from 'ol/style/fill';
import Circle from 'ol/style/circle';
import Stroke from 'ol/style/stroke';
import Overlay from 'ol/overlay';
import coordinate from 'ol/coordinate';
import Feature from 'ol/feature';
import Point from 'ol/geom/point';
import IconStyle from 'ol/style/icon';

var customStyleFunction = function(feature, resolution) {
  if(feature.values_.no2 >= '40') {
    var color = 'red'
  } 
  else if(feature.values_.no2 == '30') {
    var color = 'orange'
  } 
  else if(feature.values_.no2 == '20') {
    var color = 'orange'
  }
  else if(feature.values_.no2 == '10') {
    var color = 'green'
  }
  else if(feature.values_.no2 == '0') {
    var color = 'green'
  }
  else if(feature.values_.no2 == undefined) {
    var color = 'green'
  }

  return [new Style({
    image: new Circle({
      fill: new Fill({
        color: color
      }),
      stroke: new Stroke({
        color: 'rgba(0, 0, 0, 0.7)',
        width: 1
      }),
      radius: 5
    })
  })];
};

var backgroundStyleFunction = function(feature, resolution) {
  if(feature.values_.no2 > '70') {
    var color = 'rgba(0, 0, 0, 1)'
  }
  else if(feature.values_.no2 == '60') {
    var color = 'rgba(0, 0, 0, 0.9)'
  }
  else if(feature.values_.no2 == '50') {
    var color = 'rgba(0, 0, 0, 0.8)'
  }
  else if(feature.values_.no2 == '40') {
    var color = 'rgba(0, 0, 0, 0.7)'
  } 
  else if(feature.values_.no2 == '30') {
    var color = 'rgba(0, 0, 0, 0.5)'
  } 
  else if(feature.values_.no2 == '20') {
    var color = 'rgba(0, 0, 0, 0.3)'
  }
  else if(feature.values_.no2 == '10') {
    var color = 'rgba(0, 0, 0, 0.1)'
  }
  else if(feature.values_.no2 == '0') {
    var color = 'rgba(0, 0, 0, 0)'
  }
  else if(feature.values_.no2 == undefined) {
    var color = 'none'
  }

  return [new Style({
      fill: new Fill({
        color: color
      })
  })]}

var maternelles = new VectorLayer({
  source: new VectorSource({
    format: new GeoJSON(),
    url: 'data/final/maternelles.geojson'
  }),
  style: customStyleFunction
});
var primaires = new VectorLayer({
  source: new VectorSource({
    format: new GeoJSON(),
    url: 'data/final/primaires.geojson'
  }),
  style: customStyleFunction
});
var secondaires = new VectorLayer({
  source: new VectorSource({
    format: new GeoJSON(),
    url: 'data/final/secondaires.geojson'
  }),
  style: customStyleFunction
});
var geotiffExp = new VectorLayer({
  source: new VectorSource({
    format: new GeoJSON(),
    url: 'data/final/shapes5.geojson'
  }),
  style: backgroundStyleFunction
});

const map = new Map({
  target: 'map-container',
  projection: 'EPSG:3857',
  layers: [
    new TileLayer({
      source: new XYZSource({
        url: 'https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png',
        attributions: [ol.source.OSM.ATTRIBUTION, 'Tiles courtesy of <a href="https://geo6.be/">GEO-6</a>']
      })
    }),
    geotiffExp,
    maternelles,
    primaires,
    secondaires
  ],
  view: new View({
    center: proj.fromLonLat([4.6725, 50.5390]), //50.85,4.35
    //50.8362" N, 4.3145" E
    zoom: 8
  })
});

map.on('click', function(e) {
  overlay.setPosition();
  var features = map.getFeaturesAtPixel(e.pixel);
  if (features) {
    var increments = [];
    var pp = 0;
    for (var i = 0; i < features.length; i++) {
      var p = features[i].values_.no2;
      // console.log(p);
      increments.push(p);
    }
    var largest = 0;
    for (i=0; i<increments.length; i++) {
      if (increments[i]>largest) {
        largest = increments[i];
      }
    }
    console.log('final: ' + largest);
    // var no2 = largest;
    var nomEcole = features[0].values_.NOM_ETAB_L;
    var commune = features[0].values_.CODE_POST;
    var localite = features[0].values_.NOM_LOC;
    var rue = features[0].values_.ADR_RUE;
    var num = features[0].values_.ADR_NO;
    console.log(features);
    var no2 = features[0].values_.no2;
    var type_inst = features[0].values_.type_inst
    // console.log(no2)
    var coords = features[0].getGeometry().getCoordinates();
    var hdms = coordinate.toStringHDMS(proj.toLonLat(coords));
    //overlay.getElement().innerHTML = '<b>' + nomEcole + '</b>' + '<br>' + commune + '<br>' + localite + '<br>' + rue + ' ' + num + '<br>' + '<br>' + '<h4>Niveau de NO²: ' + largest + 'μg/m³';
    overlay.setPosition(coords);
    var sidebar = document.getElementById('info-stats');
    sidebar.innerHTML = '<p>' + nomEcole + '<br>' + rue + ' ' + num + '<br>' + commune + '<br>' + localite + '<br>' + '<br>' + '<code>' + type_inst + '</code>' + '<h3>Niveau de NO²: ' + largest + 'μg/m³</h3>' + '</p>';
  }
});

navigator.geolocation.getCurrentPosition(function(pos) {
  const coords = proj.fromLonLat([pos.coords.longitude, pos.coords.latitude]);
  map.getView().animate({center: coords, zoom: 10});
});

var overlay = new Overlay({
  element: document.getElementById('popup-container'),
  positioning: 'bottom-center',
  offset: [0, 22]
});
map.addOverlay(overlay);
// geotiffExp.setStyle(new Style({
//   stroke: new Stroke({
//     width: 1.5,
//     color: 'black'
//   }),
//   fill: new Fill({
//     color: 'rgba(0, 0, 0, 0.8)'
//   })
// }));