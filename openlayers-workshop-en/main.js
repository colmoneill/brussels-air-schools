import 'ol/ol.css';
import Map from 'ol/map';
import View from 'ol/view';
import TileLayer from 'ol/layer/tile';
import XYZSource from 'ol/source/xyz';
import proj from 'ol/proj';
import VectorLayer from 'ol/layer/vector';
import VectorSource from 'ol/source/vector';
import GeoJSON from 'ol/format/geojson';
import Style from 'ol/style/style';
import Fill from 'ol/style/fill';
import Stroke from 'ol/style/stroke';
import Overlay from 'ol/overlay';
import coordinate from 'ol/coordinate';
import Feature from 'ol/feature';
import Point from 'ol/geom/point';

const key = 'pk.eyJ1IjoiY29sbW9uZWlsbCIsImEiOiJjamQyN3lkMWEwaHhoMnFsb2hjZmR2N2t4In0.cgA-n09im4Q1MiA_CU8QiQ';

function transform(extent) {
  return ol.proj.transformExtent(extent, 'EPSG:4326', 'EPSG:3857');
}

var extents = {
    Belgium: transform([2.47977628893, 49.4611053717, 6.49409016877, 51.5415540572])
};

// const pollution = new XYZSource({
//     url: 'http://localhost/tiles/{z}/{x}/{y}.png',
// });
//
// const pollution2 = new XYZSource({
//     extent: [2.47977628893, 49.4611053717, 6.49409016877, 51.5415540572],
//     url: '../geotiff-tiles/{z}/{x}/{y}.png',
// });

const elevation = new XYZSource({
  url: 'https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=' + key,
  crossOrigin: 'anonymous'
});

const map = new Map({
  target: 'map-container',
  projection: "EPSG:3857",
  layers: [
    // new TileLayer({
      // source: new XYZSource({
        // url: 'http://tile.stamen.com/terrain/{z}/{x}/{y}.jpg'
      // })
    // }),
    new TileLayer({
      source: new XYZSource({
        url: "https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png",
        attributions: [ ol.source.OSM.ATTRIBUTION, "Tiles courtesy of <a href=\"https://geo6.be/\">GEO-6</a>" ]
      })
    }),
    // new VectorLayer({
    //   source: new VectorSource({
    //     format: new GeoJSON(),
    //     url: 'data/20step-mercat.geojson',
    //   }),
    //   style: new Style({
    //     fill: new Fill({
    //       color: 'red'
    //     }),
    //     stroke: new Stroke({
    //       color: 'black'
    //     })
    //   })
    // }),
    new VectorLayer({
      source: new VectorSource({
        format: new GeoJSON(),
        url: 'data/qgis-maternel-spec-export.geojson',
      }),
    }),
    // new VectorLayer({
      // source: new VectorSource({
        // format: new GeoJSON(),
        // url: 'data/countries.json',
      // }),
      // style: new Style({
        // stroke: new Stroke({
          // color: 'black'
        // })
      // })
    // }),
    // new TileLayer({
        // opacity: 0.8,
        // source:  elevation
    // }),
  ],
  view: new View({
    center: proj.fromLonLat([4.6725, 50.5390]), //50.85,4.35
    //50.8362" N, 4.3145" E
    zoom: 8
  })
});

map.on('click', function(e) {
  overlay.setPosition();
  var features = map.getFeaturesAtPixel(e.pixel);
  if (features) {
    var coords = features[0].getGeometry().getCoordinates();
    var hdms = coordinate.toStringHDMS(proj.toLonLat(coords));
    overlay.getElement().innerHTML = hdms;
    overlay.setPosition(coords);
  }
});

navigator.geolocation.getCurrentPosition(function(pos) {
  const coords = proj.fromLonLat([pos.coords.longitude, pos.coords.latitude]);
  map.getView().animate({center: coords, zoom: 10});
});

var overlay = new Overlay({
element: document.getElementById('popup-container'),
positioning: 'bottom-center',
offset: [0, -10]
});
map.addOverlay(overlay);

const position = new VectorSource();
const vector = new VectorLayer({
  source: position
});
map.addLayer(vector);

position.addFeature(new Feature(new Point(coords)));

vector.setStyle(new Style({
  image: new IconStyle({
    src: './data/marker.png'
  })
}));
