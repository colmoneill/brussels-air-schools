import csv
import sys
import geocoder
import time

ifile  = open(sys.argv[1])
reader = csv.reader(ifile)
ofile  = open(sys.argv[2], "w")
writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

rownum = 0
for row in reader:
    # Save header row.
    if rownum == 0:
        header = row
        print(header)
        header.extend(['lat', 'lng'])
        print(header)
        writer.writerow(row)

    else:
        time.sleep(1)
        colnum = 0
        #print(row[0])
        query = row[1] + ', ' + row[2] + ', ' + row[3] + ', ' + row[4]
        print('query= ' + query)

        g = geocoder.osm(query)
        if g.ok is False:
            #print(row[0])
            print(query + 'no result with OSM')
            #print(g.json)

            g = geocoder.google(query)
            if g.ok is False:
                print(query + 'returns no result with google')
                g = geocoder.arcgis(query)
               
                if g.ok is False:
                    print('NO RESULTS USING OSM THEN GOOGLE THEN ARCGIS')
                else:
                    print('result found with ARCGIS: ')
                    print(g.latlng)
            print('final result:')
            print(g.latlng)

        print(g.latlng)
        #print(row)
        row.extend([g.lat, g.lng])
        print(row)
        writer.writerow(row)
        #for col in row:
            #print(col)
        #    colnum += 1

    rownum += 1
    print('line:' + str(rownum))

ifile.close()
ofile.close()
