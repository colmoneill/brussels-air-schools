import csv
import geocoder
import json

ifile  = open('/home/colm/git/OSP/medor.carto.pollution/export_etablissements/maternel_specialise-utf8.csv')
reader = csv.reader(ifile)
ofile  = open('/home/colm/git/OSP/medor.carto.pollution/export_etablissements/utf8/maternel_specialise-utf8-latlng.csv', "w")
writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

rownum = 0
for row in reader:
    # Save header row.
    if rownum == 0:
        header = row
        #print(header)
        header.extend(['lat', 'lng'])
        #print(header)
        #writer.writerow(row)

    else:
        colnum = 0
        print(row[0])
        query = row[1] + ', ' + row[2] + ', ' + row[3] + ', ' + row[4]
        #print(query)
        g = geocoder.osm(query)

        if g.ok is False:
            #print(row[0])
            #print(query)
            #print(g.json)
            g = geocoder.google(query)
            #print(g.json)

        #print(g.geojson)
        geocoder_json = g.geojson
        json_data = json.load(geocoder_json)
        print(json_data)
        #print(row)
        #row.extend([g.lat, g.lng])
        #print(row)
        #writer.writerow(row)
        #for col in row:
            #print(col)
        #    colnum += 1

    rownum += 1

ifile.close()
ofile.close()
